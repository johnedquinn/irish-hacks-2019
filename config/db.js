/*
 * File: db.js
 * Author: CLEJ
 * Description: connects the application to the mongoDB database
*/

const mongoose = require('mongoose');
const config = require('config');

// Get value contained in mongoURI object in default.json
const db = config.get('mongoURI');

const connectDB = async () => {
    try {
        // Attempt connection
        await mongoose.connect(db, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        });
        console.log('MongoDB Connected...');
    } catch (err) {
        console.error(err.message);

        // Exit process with failure
        process.exit(1);
    }
};

module.exports = connectDB;
