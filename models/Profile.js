/*
 * File: Profile.js
 * Author: xxx
 * Description: defines the profile schema
*/
const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
    },
    bio: {
        type: String,
    },
    isVolunteer: {
        type: Boolean,
        required: true
    },
    skills: [
        {
            type: String
        }
    ]
})

module.exports = Profile = mongoose.model('profile', ProfileSchema);
