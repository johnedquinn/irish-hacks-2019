/*
 * File: Post.js
 * Author: xxx
 * Description: defines the post schema
*/
const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
    },
    title: {
        type: String,
        required: true
    },
    desc: {
        type: String,
        required: true
    },
    volunteers: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user'
        }
    ],
    resolved: {
        type: Boolean,
        default: false
    }
})

module.exports = Post = mongoose.model('post', PostSchema);
