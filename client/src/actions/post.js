import axios from 'axios';
import {setAlert} from './alert';
import{
  GET_POSTS,
  GET_POST,
  POST_ERROR,
  POST_SUCCESS,
  POST_FAIL,
  UPDATE_LIKES
}from './types';

//GET posts
export const getPosts = () => async dispatch =>{
  try {
    const res = await axios.get('/api/posts');

    dispatch({
      type: GET_POSTS,
      payload: res.data
    });
  } catch(err){
    dispatch({
      type: POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status}
    });
  }
}

//Add like
export const addLike = postId => async dispatch =>{
  try {
    const res = await axios.post(`/api/posts/volunteer/${postId}`);

    dispatch({
      type: UPDATE_LIKES,
      payload: {postId, likes: res.data}
    });
  } catch(err){
    dispatch({
      type: POST_ERROR,
      payload: { msg: err.response.statusText, status: err.response.status}
    });
  }
};

export const getPost = (post_id) => async dispatch =>{
  try {
    const res = await axios.get(`/api/posts/0/${post_id}`);

    dispatch({
      type: GET_POST,
      payload: res.data
    });
  } catch(err){
    dispatch({
      type: POST_FAIL,
      payload: { msg: err.response.statusText, status: err.response.status}
    })
  }
}

export const createPost = (title, desc) => async dispatch => {
  const config ={
    headers: {
      'Content-Type': 'application/json'
    }
  }
  const body = JSON.stringify({title, desc});

  try{
    const res = await axios.post('/api/posts', body, config);
    dispatch({
      type: POST_SUCCESS,
      payload: res.data
    });

    //dispatch(loadUser());
  } catch(err) {
    const errors = err.response.data.errors;

    if(errors) {
      errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
    }

    dispatch({
      type: POST_FAIL
    });
  }
}
