import {
  GET_POSTS,
  GET_POST,
  POST_ERROR,
  POST_SUCCESS,
  POST_FAIL,
  UPDATE_LIKES
} from '../actions/types';

const initialState = {
  posts: [],
  post: null,
  loading: true,
  error: {},
  postCreated: null
}

export default function (state = initialState, action) {
  const {type,payload} = action;

  switch(type){
    case GET_POSTS:
      return{
        ...state,
        posts: payload,
        loading: false,
        postCreated: false
      }
    case GET_POST:
    case POST_SUCCESS:
      return{
        ...state,
        post: payload,
        loading: false,
        postCreated: true
      }
    case POST_FAIL:
      return{
        ...state,
        post: payload,
        loading: false,
        postCreated: false
      }
    case POST_ERROR:
      return{
        ...state,
        posts: payload,
        loading: false,
        postCreated: false
      }
    case UPDATE_LIKES:
      return{
        ...state,
        posts: state.posts.map(post => post._id === payload.id ? {...post, likes: payload.likes } : post),
        loading: false
       }
    default:
      return state;
  }
};
