import React, {Fragment, useState} from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import {connect} from 'react-redux';
import {addLike} from '../../actions/post';


const PostItem = ({ addLike, auth, post:{_id, user, title, desc, volunteers, date}}) => {
  const avatar = user.avatar;
  const name = user.name;

  const [formData] = useState({
    length: volunteers.length,
  });

  var { length } = formData;

  const onSubmit = async e => {
    e.preventDefault();
      length++;
    };

  return (
      <div className="post bg-white p-1 my-1">
        <div>
          <a href="profile.html">
            <img
              class="round-img"
              src={avatar}
              alt=''
            />
            <h4>{name}</h4>
          </a>
        </div>
        <div>
          <p class="my-1">
            <Link to={`/post/${_id}`}>{title}</Link>
          </p>
          <p class="my-1">
            {desc}
          </p>
           <p class="post-date">
              Posted on <Moment format = 'MM/DD/YYYY'>{date}</Moment>
          </p>
          <button onClick={e=> {addLike(_id); window.location.reload();} }type="button" class="btn btn-light">
            <i class="fas fa-thumbs-up"> {' '}</i>
            <span> {length}</span>
          </button>
        </div>
      </div>
  );
};

PostItem.propTypes={
  post: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { addLike })(PostItem);
