import React, {Fragment, useEffect } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import {getPosts} from '../../actions/post';
import PostItem from './PostItem';
import { Link } from 'react-router-dom';

const Posts = ({getPosts, post:{posts, loading}}) =>{

  useEffect(() => {
    getPosts();
  }, [getPosts]);


  //return <div>Posts</div>;
  return <Fragment>
    <h1 className = "large text-primary">Posts</h1>
    <p className="lead">
      <i className= "fas.fa-user"></i> Your feed:
    </p>
    <p className="my-1">
      <Link to="/Postform">Create a Post</Link>
    </p>
    <div className="posts">
      {[...posts].reverse().map(post => (
        <PostItem key={post._id} post={post} />
      ))}
    </div>
  </Fragment>;
//make a list of different items
}

Posts.propTypes= {
  getPosts: PropTypes.func.isRequired,
  post: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  post: state.post
});

export default connect(mapStateToProps, {getPosts})(Posts);
