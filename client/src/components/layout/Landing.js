import React from 'react'
import {Link} from 'react-router-dom';

const Landing = () => {
  return (
    <section className="landing">
      <div className="dark-overlay">
        <div className="landing-inner">
          <h1 className="x-large">Samaritan</h1>
          <p className="lead">
            Helping the elderly live more fruitful lives through assistance with small tasks and light conversation!
          </p>
          <div className="buttons">
            <Link to="/register" className="btn btn-primary">
              Sign Up
            </Link>
            <Link to="/login" className="btn btn-light">
              Login
            </Link>
          </div>
        </div>
      </div>
    </section>
  )
};

export default Landing;
