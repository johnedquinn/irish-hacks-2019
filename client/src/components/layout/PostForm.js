import React, { Fragment, useState } from 'react'
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { createPost } from '../../actions/post';

// @name: Login
// @desc: Performs all the login procedures
const PostForm = ({createPost, isAuthenticated, postCreated}) => {
  const [formData, setFormData] = useState({
    title: '',
    desc: '',
  });

  const { title, desc } = formData;

  // @name: onChange
  // @desc: N/A
  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  // @name: onSubmit
  // @desc: Gets called when submit button is pressed
  const onSubmit = async e => {
    e.preventDefault();
      createPost(title, desc);
    };

  if (postCreated){
    return <Redirect to='/posts' />
  }

    // Redirect if logged in
    /*if(!isAuthenticated){
      return <Redirect to='/login' />
    }*/

  // Return Login page
  return(
    <Fragment>
      <h1 className="large text-primary">Create a Post</h1>
        <p className="lead">
          <i className="fas fa-user"></i> Create a Post
        </p>
        <form className="form" onSubmit = {e => onSubmit(e)}>

          <div className="form-group">
            <input
              type="text"
              placeholder="Title"
              name="title"
              value ={title}
              onChange = { e => onChange(e) }
              required
            />
          </div>
        <div className="form-group">
          <input
            type="text"
            placeholder="Description"
            name="desc"
            value ={desc}
            onChange = { e => onChange(e)}
          />
        </div>
        <input type="submit" className="btn btn-primary" value="Submit" />
        </form>
      </Fragment>
  );
};

// Declares the proptypes to be passed in
PostForm.propTypes = {
  createPost: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
  postCreated: PropTypes.bool
}

// NA
const mapStateToProps = state =>({
  isAuthenticated: state.auth.isAuthenticated,
  postCreated: state.post.postCreated
});

export default connect(mapStateToProps, { createPost })(PostForm);
