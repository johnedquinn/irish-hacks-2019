import React, { Fragment, useState, useEffect } from 'react'
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getPost } from '../../actions/post';
import Moment from 'react-moment';

// @name: Login
// @desc: Performs all the login procedures
const Post = ({getPost, post:{post, loading}, match}) => {

  useEffect(() => {
    getPost(match.params.post_id);
  }, [getPost]);

  if (loading || post == null) return <div></div>;

  const { title, desc, date, volunteers, _id} = post;
  const {name, avatar, email, phone, address} = post.user;

  return (
      <Fragment>
          <div>
            <h4>{name}</h4>
            <p class="my-1">
              <Link to={`/post/${_id}`}>{title}</Link>
            </p>
            <p class="my-1">
              {desc}
            </p>
            <h2>Contact</h2>
            <p class="my-1">
              Email: {email}
            </p>
            <p class="my-1">
              Mobile: {phone}
            </p>
             <p class="post-date">
                Posted on <Moment format = 'MM/DD/YYYY'>{date}</Moment>
            </p>
            <button type="button" class="btn btn-light">
              <i class="fas fa-thumbs-up"> {' '}</i>
              <span> {volunteers.length}</span>
            </button>
          </div>
          <div>
            <a href="profile.html">
              <img
                class="round-img"
                src={avatar}
                alt=''
              />
            </a>
          </div>
        </Fragment>
    );
};

Post.propTypes={
  post: PropTypes.object.isRequired,
  getPost: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  post: state.post
});

export default connect(mapStateToProps, { getPost })(Post);
