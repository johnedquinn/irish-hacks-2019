/*
 * File: posts.js
 * Author: XXX
 * Description: NA
*/
const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');
//const request = require('request');
const config = require('config');
const jwt = require('jsonwebtoken');
const Post = require('../../models/Post');
const User = require('../../models/User');

// GET /api/posts/ --> all posts
// GET /api/posts/:USER_ID/:POST_ID --> specific post
// GET /api/posts/:USER_ID

// @route   GET api/profile/me
// @desc    Get current user's profile
// @access  Private
/*router.get('/me', auth, async (req, res) => {
    try {
        // Check if profile exists
        const volunteer = await Profile.findOne({ user: req.user.id }).populate('user', ['name', 'avatar']); // @TODO: wtf populate
        if (!volunteer) {
            return res.status(400).json({ msg: 'There is no info for this volunteer.' });
        }

        return res.json(volunteer);
    } catch (err) {
        console.log(err.message);
        return res.status(500).send("Server Error");
    }
});*/

// @route   POST api/posts
// @desc    Create a post
// @access  Private
router.post('/',[
    //check('user', 'user is required').not().isEmpty(),  // I dob't think we need this
    check('title', 'title is required').not().isEmpty(),
    check('desc', 'description is required').not().isEmpty()
], auth, async (req, res) => {
    // Check if request syntax is matched
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    // Pull variables out
    const {
        user,
        title,
        desc,
        resolved,
        volunteers
    } = req.body;

    // Build post object
    const postFields = {};
    postFields.user = req.user.id;
    postFields.title = title;
    postFields.desc = desc;
    postFields.resolved = resolved;
    postFields.volunteers = [];

    try {
        // If profile doesn't exist, create a profile
        post = new Post(postFields);
        await post.save();

        return res.json(post);

    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }
});

// @route   GET api/posts
// @desc    Get all posts
// @access  Public
router.get('/', async (req, res) => {
    try {
        // Get all profiles
        const posts = await Post.find().populate('user', ['name', 'avatar']);
        return res.json(posts);
    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }
});

// @route   GET api/posts/:user_id/:post_id
// @desc    Get specific post
// @access  Public
router.get('/:user_id/:post_id', async (req, res) => {
    try {
        // Look for profile with user id
        const post = await Post.findById(req.params.post_id).populate('user', ['name', 'avatar', 'address', 'email', 'phone']);
        if (!post) {
            return res.status(400).json({ msg: 'Post not found.' });
        }
        return res.json(post);
    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Post not found.' });
        }
        return res.status(500).send('Server Error');
    }
});

// @route   GET api/posts/:user_id
// @desc    Get all posts from a user
// @access  Public
router.get('/:user_id', async (req, res) => {
    try {
        // Look for profile with user id
        const post = await Post.find({ user: req.params.user_id }).populate('user', ['name', 'avatar']);
        if (!post) {
            return res.status(400).json({ msg: 'Post not found.' });
        }
        return res.json(post);
    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Post not found.' });
        }
        return res.status(500).send('Server Error');
    }
});

// @route   DELETE api/posts/:post_id
// @desc    Delete volunteer, user, and post
// @access  Private
router.delete('/:user_id/:post_id', auth, async (req, res) => {
    try {
        // TODO : Remove user's posts

        // Remove profile
        const post = await Post.findById(req.params.post_id );
        console.log(post);
        if (!post) {
            return res.status(400).json({ msg: 'Post not found.' });
        }
        if (req.user.id != post.user) {
            return res.status(401).json({ msg: 'Not matching user.' });
        }
        await post.remove();

        // Remove user
        //const user = await User.findOneAndRemove({ _id: req.user.id });

        return res.json({ msg: 'Post removed.' });
    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Post not found.' });
        }
        return res.status(500).send('Server Error');
    }
});

// @route   POST api/posts/volunteer/:post_id
// @desc    Volunteer
// @access  Public
router.post('/volunteer/:post_id', auth, async (req, res) => {
    try {
        // Find post
        const post = await Post.findById(req.params.post_id);
        if (post.volunteers.indexOf(req.user.id) > -1) {
            return res.status(400).json({ msg: 'Already volunteered.' });
        }
        post.volunteers.unshift(req.user.id);
        await post.save();
        return res.json(post);
    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }
});

// @route   GET api/posts/unvolunteer/:post_id
// @desc    Unvolunteer
// @access  Public
router.post('/unvolunteer/:post_id', auth, async (req, res) => {
    try {
        // Find post
        const post = await Post.findById(req.params.post_id);
        if (post.volunteers.indexOf(req.user.id) < 0) {
            return res.status(400).json({ msg: 'Have not volunteered yet.' });
        }
        // Remove volunteer
        const removeIndex = post.volunteers.map(user => user.toString()).indexOf(req.user.id);
        if (removeIndex < 0) {
            return res.status(400).json({ msg: 'User not found in likes.' });
        }
        post.volunteers.splice(removeIndex, 1);
        await post.save();
        return res.json(post);
    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }
});

module.exports = router;
