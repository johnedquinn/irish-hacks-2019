/*
 * File: volunteers.js
 * Author: XXX
 * Description: NA
*/
const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');
//const request = require('request');
const config = require('config');
const jwt = require('jsonwebtoken');
const Profile = require('../../models/Profile');
const User = require('../../models/User');

// @route   GET api/profile/me
// @desc    Get current user's profile
// @access  Private
router.get('/me', auth, async (req, res) => {
    try {
        // Check if profile exists
        const volunteer = await Profile.findOne({ user: req.user.id }).populate('user', ['name', 'avatar']); // @TODO: wtf populate
        if (!volunteer) {
            return res.status(400).json({ msg: 'There is no info for this volunteer.' });
        }

        return res.json(volunteer);
    } catch (err) {
        console.log(err.message);
        return res.status(500).send("Server Error");
    }
});

// @route   POST api/profile
// @desc    Create or update a volunteer profile
// @access  Private
router.post('/',[
    check('user', 'user is required').not().isEmpty(),  // I dob't think we need this
    check('isVolunteer', 'isVolunteer is required').not().isEmpty()
    //check('skillset', 'Skillset is required').not().isEmpty(),
], auth, async (req, res) => {
    // Check if request syntax is matched
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    // Pull variables out
    const {
        user,
        skills,
        bio,
        isVolunteer,
    } = req.body;

    // Build profile object
    const profileFields = {};
    profileFields.user = req.user.id;
    profileFields.bio = bio;
    profileFields.isVolunteer = isVolunteer;

    if (skills) {
        profileFields.skills = skills.split(',').map(skill => skill.trim());
    }

    try {
        // If profile exists, update
        let profile = await Profile.findOne({user: req.user.id});
        if (profile) {
            profile = await Profile.findOneAndUpdate(
                { user: req.user.id },
                { $set: profileFields },
                { new: true }
            );
            return res.json(profile);
        }

        // If profile doesn't exist, create a profile
        profile = new Profile(profileFields);
        await profile.save();

        return res.json(profile);

    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }
});

// @route   GET api/profile
// @desc    Get all profiles
// @access  Public
router.get('/', async (req, res) => {
    try {
        // Get all profiles
        const profiles = await Profile.find().populate('user', ['name', 'avatar']);
        return res.json(profiles);
    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }
});

// @route   GET api/profile/:user_id
// @desc    Get profile by user id
// @access  Public
router.get('/:user_id', async (req, res) => {
    try {
        // Look for profile with user id
        const profile = await Profile.findOne({ user: req.params.user_id }).populate('user', ['name', 'avatar']);
        if (!profile) {
            return res.status(400).json({ msg: 'Profile not found.' });
        }
        return res.json(profile);
    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Profile not found.' });
        }
        return res.status(500).send('Server Error');
    }
});

// @route   DELETE api/profile
// @desc    Delete volunteer, user, and post
// @access  Private
router.delete('/', auth, async (req, res) => {
    try {
        // TODO : Remove user's posts

        // Remove profile
        const profile = await Profile.findOneAndRemove({ user: req.user.id });

        // Remove user
        const user = await User.findOneAndRemove({ _id: req.user.id });

        return res.json({ msg: 'User removed.' });
    } catch (err) {
        console.error(err.message);
        if (err.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Profile not found.' });
        }
        return res.status(500).send('Server Error');
    }
});

module.exports = router;
