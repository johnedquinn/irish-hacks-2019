/*
 * File: auth.js
 * Author: XXX
 * Description: NA
*/

const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @route   GET api/auth
// @desc    Get user information with token
// @access  Public
router.get('/', auth, async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        return res.status(200).json({user});
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @route   POST api/auth
// @desc    Login/authenticate user and get token
// @access  Public
router.post('/', [
    check('email', 'Please include a valid email address.').isEmail(),
    check('password', 'Password is required.').not().isEmpty()
], async (req, res) => {
    // Check for errors in request
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    // Pull out variables from HTTP request
    const { email, password } = req.body;

    try {
        // See if user exists
        let user = await User.findOne({ email });
        if (!user) {
            return res.status(400).json({ errors: [{ msg: 'Invalid Credentials' }] });
        }

        // Make sure password matches
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
            return res.status(400).json({ errors: [{ msg: 'Invalid Credentials' }] });
        }

        // Return JSON Web Token
        const payload = {
            user: {
                id: user.id,
            }
        }
        jwt.sign(payload, config.get('jwtSecret'), { expiresIn: 360000 }, (err, token) => {
            if (err) throw err;
            return res.json({ token });
        }); // @TODO: Update expiresIn option to 3600

    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }

});

module.exports = router;
