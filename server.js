/*
 * File: server.js
 * Author: CLEJ
 * Description: starts and runs the server
*/

const express = require('express');
const connectDB = require('./config/db');

const app = express();

// Connect to MongoDB Database
connectDB();

// Init Middleware --> Allows us to get the data in req.body
app.use(express.json({ extended: false }));

app.get('/', (req, res) => res.send('API Running'));

// Define routes
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/users', require('./routes/api/users'));
app.use('/api/profiles', require('./routes/api/profiles')); // @TODO: add more routes
app.use('/api/posts', require('./routes/api/posts'));

// Look for environment variable PORT to use. If not specified, use 5000
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
});
