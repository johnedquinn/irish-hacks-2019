# Irish Hacks 2019
Hackathon held at the University of Notre Dame. The group members are as follows:
- John Quinn (johnedquinn)
- Emma Ascolese (emmaascolese)
- Carlos Murillo (cmurillo9)
- Livia Johan (liviajohan)

## Project Overview
This hackathon had the aims of providing support for social good. This project, specifically, set out to help elderly people reach out to younger neighbors for help with small tasks such as mowing the lawn, changing lightbulbs, sharing coffee, etc.

## Project Details
This project manipulates the MERN (Mongoose, Express, React, Node) stack to implement the functionality of this web-based project.

## Build Setup
```bash
# To run the server and client concurrently
npm dev
```